public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private int salary;

    public Employee(int id, String name, String lastName, int salary){
        this.id = id;
        this.firstName = name;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int raiseSalary(int precent){
        return this.salary = (salary + (salary*precent)/100);
    }
    public int reduceSalary(int precent){
        return this.salary = (salary + (salary*precent)/100);
    }
    public int getAnnualSalary(){
        return salary*12;
    }
    public void setId (int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
    public void setFirstName(String firstName){
        this.firstName = firstName;
    }
    public String getFirstName(){
        return firstName;
    }
    public void setLastName(String lastName){
        this.lastName = lastName;
    }
    public String getLastName(){
        return lastName;
    }
    public void setSalary(int salary){
        this.salary = salary;
    }
    public int getSalary(){
        return salary;
    }
    public String toString(){
        return "Isci[ id = " +getId()+ ", name = " + getFirstName() +", maas = " + getSalary() + "]";
    }
}
